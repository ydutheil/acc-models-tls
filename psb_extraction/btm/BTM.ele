option, -echo;
option, info;
option, warn;
!--------------------------------------------------------------------------------------------
! - The lengths of BT.BHZ10, BTM.BHZ10, QNOs, DVT and DHZ is the magnetic length
!
! - For BT.BHZ10 and BTM.BHZ10, L corresponds to the beam path length
!
!--------------------------------------------------------------------------------------------
! Directory: /afs/cern.ch/eng/ps/cps/TransLines/PSB-PS/2014/elements
! Element file created in June 2014 by O.Berrig, G.P. Di Giovanni, V. Raginel and B. Mikulec
!--------------------------------------------------------------------------------------------
!
! Changes Feb 17 (C. Hessler):
! - magnetic lengths of BTM.DVT10 and BTM.DHZ10: 0.300 m -> 0.436 m
! - length BT.BHZ10 2.2m -> 2.0m
! - length BPMs -> 0.328m
! - length BTM.BHZ10
!
! Changes 14/11/2018 (C. Hessler)
! - Replace BPM respresentation in sequence file:
!   BPM with real flange-to-flange length -> BPM with length 0 (at center of electrodes)
!--------------------------------------------------------------------------------------------

BT.BHZ10      : SBEND,       L:=2.000457493, ANGLE:= dBTBHZ10,E1=0,E2:= dBTBHZ10,FINT=0.46, HGAP=.128/2;
// The magnetic length has been reduced from 2.2 m to 2.0 m. To keep the downstream elements at the same survey
// coordinates, the increased path length in the arrangement with the shortened BHZ10 must be taken into account:
// Therefore BT.BHZ10 must be shifted downstream by btbhz10.ds and all following elements by 2*btbhz10.ds.
btbhz10.ds=0.099668699/cos(dBTBHZ10/2)-(dBTBHZ10/2)*0.099668699/sin(dBTBHZ10/2);

BTM.VVS10    :  MARKER;
BTM.BTV10    :  MONITOR, L:=0;
BTM.QNO05    :  QUADRUPOLE,  L= 0.56,  K1 := KBTMQNO05;

! NEW: Effective length (proton path length in magnet) for 2.0 GeV: L=2.324 (for 1.4 GeV: L=2.330)
BTM.BHZ10    :  SBEND,       L:= 2.324 ,ANGLE := dBTMBHZ10, E1:= dBTMBHZ10/2, E2:=dBTMBHZ10/2, FINT=0.57, HGAP=.098/2;

! Lmag(new) = 2.324/dBTMBHZ10*2*sin(dBTMBHZ10/2) = 2.312106279353731
! (Lmag(old)-Lmag(new))/2 = (2.339 - 2.312106279353731)/2 = 0.013446860323134
btmbhz10.ds=0.013446860323134/cos(dBTMBHZ10/2)-(dBTMBHZ10/2)*0.013446860323134/sin(dBTMBHZ10/2);

BTM.QNO10    :  QUADRUPOLE,  L= 0.56,  K1 := KBTMQNO10;
BTM.DVT10    :  VKICKER,     L= 0.300, KICK  := dBTMDVT10;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
!BTM.DVT10    :  VKICKER,     L= 0.436, KICK  := dBTMDVT10;!NEW
BTM.DHZ10    :  HKICKER,     L= 0.300, KICK  := dBTMDHZ10;! correction to be consistent with drawing PSBIHENS0031 (2017), CH 16/11/2018
!BTM.DHZ10    :  HKICKER,     L= 0.436, KICK  := dBTMDHZ10;!NEW
BTM.QNO20    :  QUADRUPOLE,  L= 0.56,  K1 := KBTMQNO20;
BTM.BPM00    :  MONITOR,L:= 0;
BTM.BPM10    :  MONITOR,L:= 0;
BTM.BTV15    :  MONITOR, L:=0;
BTM.VPI11    :  MARKER;
!BTY.HOP	     :  MARKER;! marker for BTY hand over point (upstream flange of BTY.BVT101), CH 16/11/2018
BTM.VC       :  MARKER;
BTM.VPI11A   :  MARKER;
BTM.BSGH01   :  MONITOR, L:=0;
BTM.BSGV01   :  MONITOR, L:=0;
BTM.BSGH02   :  MONITOR, L:=0;
BTM.BSGV02   :  MONITOR, L:=0;
BTM.BTV20    :  MONITOR, L:=0;
BTM.BSGH03   :  MONITOR, L:=0;
BTM.BSGV03   :  MONITOR, L:=0;
BTM.BCT10    :  MONITOR, L:=0;
BTM.TDU10.ENTRYFACE : MARKER;
BTM.TDU10    :  MARKER;

! According to NORMA database and the EDMS document 1159505, Lmag = 1.076/0.945= 1.138624 m
! The deflection angle to send the beam toward BTY is dBTYBVT101 = 0.199995 rad
! The magnet is tilted vertically such that E1=E2=dBTYBVT101/2
!BTY.BVT101   :  PLACEHOLDER, L=1.138624/cos(0.199995/2) ;

! redefined length, since for BTM aperture calculations the length of the BTY.BVT101 vacuum chamber is relevant and not the magnetic length
!BTY.BVT101   :  PLACEHOLDER, L:= 1.921;! redefined CH 16/11/2018
BTY.BVT101   :  PLACEHOLDER, L:= 0;

BTY.START : MARKER;

option, warn;
option, -echo;
option, -info;
return;
