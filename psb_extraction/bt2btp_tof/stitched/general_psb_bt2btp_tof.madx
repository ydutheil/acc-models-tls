!==============================================================================================
! MADX file for PSB-BT2-BTP TOF optics
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

 title, "PSB/BT2/BTP optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Cleaning .inp output files
***************************************/

system, "rm *.inp";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../bt bt_repo";
system, "ln -fns ./../../btp btp_repo";

! When running the stitched model with PS these symbolic links will be made instead

if (stitchps == 1){
system, "ln -fns ./../../../psb_extraction/bt bt_repo";
system, "ln -fns ./../../../psb_extraction/btp btp_repo";
};



/*******************************************************************************
 * Beam command
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, PC = 2.794987;
 BRHO := BEAM->PC * 3.3356;

/*******************************************************************************
 * Macros
 *******************************************************************************/
 
set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

write_ini_conditions(xtlgeode, pxtlgeode, beamname,filename) : macro = {

betx0 = beamname->betx;
bety0 =  beamname->bety;

alfx0 = beamname->alfx;
alfy0 = beamname->alfy;

dx0 = beamname->dx;
dy0 = beamname->dy;

dpx0 = beamname->dpx - pxtlgeode/(beam->beta);
dpy0 = beamname->dpy;

x0 = beamname->x - xtlgeode;
y0 = beamname->y;

px0 = beamname->px - pxtlgeode;
py0 = beamname->py;

mux0 = beamname->mux;
muy0 = beamname->muy;

assign, echo="filename";

print, text="/*********************************************************************";
print, text="Initial conditions from MADX model of PSB extraction to BT lines";
print, text="*********************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

value,x0 ;
value,px0 ;

assign, echo=terminal;

};


/*****************************************************************************
 Calculate initial conditions for BT transfer line
*****************************************************************************/

call, file = "bt_repo/load_psb2_extraction_tof.madx";

! In this example the KFA14's kick can be adjusted (absolute error  in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! If "sign" (second argument) = 1, positive kick, negative otherwise

 set, format="22.10e";
exec, calculate_extraction(1e-3, 1, twiss_psb_stitched);    

/***********************************************************
* Define TL reference frame - to be updated with SU (GEODE) measured values
************************************************************/

! Until we have better information we assume the beam is aligned to BT in the nominal case
xtl = tl_initial_cond_nominal->x;
pxtl = tl_initial_cond_nominal->px;

/*****************************************************************************
 * BT2 and BTP
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 
 option, -echo;
 call, file = "bt_repo/BT-BTP-LIU.str";
 call, file = "bt_repo/HI-LIU.str";
 call, file = "bt_repo/BT.ele";
 call, file = "bt_repo/BT2_LIU.seq";
 call, file = "bt_repo/BT.dbx"; 
 call, file = "btp_repo/BTP.ele";
 call, file = "btp_repo/BTP.seq"; 
 call, file = "btp_repo/BTP.dbx"; 
 option, echo;
 

 /*******************************************************************************
 * Combine sequences
 *******************************************************************************/

lpsbext = 139.110102;

lbt1 = 33.91783854;
lbt2 = 33.9071301;
lbt3 = 33.89615656;
lbt4 = 33.906865;

lbtp = 35.79972785;

lps = 628.3185;

bt2btp: SEQUENCE, refer=ENTRY, L  = lbt2 + lbtp;
	BT2, 	AT = 0 ;
	BTP, 	AT = lbt2;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = bt2btp;
FLATTEN;
ENDEDIT;

/***********************************************************
* Save initial parameters in PSB ring for JMAD
************************************************************/
exec, write_ini_conditions(0,0,psbstart,PSB2_START_TOF.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond,BT2_START_KICK_RESPONSE_TOF.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT2-BTP and stitch result for kick response
 *******************************************************************************/

use, sequence= bt2btp;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_kick_response, place = #e;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=trajectory, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_psb_bt2btp_tof_kick_response_complete.tfs";

/***********************************************************
* Save parameters at handover point for injection to PS for kick response
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_kick_response,PS2_START_KICK_RESPONSE_TOF.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond_nominal,BT2_START_TOF.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT2-BTP and stitch result for nominal case
 *******************************************************************************/

use, sequence= bt2btp;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_nominal, place = #e;
savebeta,label=btp2_start_tof, place = btp.start;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=nominal, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=nominal;

    i = i + 1;
};
option, info;
write, table=nominal, file="twiss_psb_bt2btp_tof_nom_complete.tfs";

/***********************************************************
* Save parameters at BTP start for nominal case
************************************************************/

exec, write_ini_conditions(0,0,btp2_start_tof,BTP2_START_TOF.inp);

/***********************************************************
* Save parameters at handover point for injection to PS for nominal case
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_nominal,PS2_START_TOF.inp);

/***********************************************************
* JMAD: prepare single sequences
************************************************************/

if (stitchps == 1){};
else {

/***********************************************************
* PI.SMH42.BTP renamed to PI.SMH42 in BTP sequence (otherwise
* YASP doesn't recognise the septum name)
************************************************************/

PI.SMH42: rbend,l=0.942,angle:= angleSMH42;

SEQEDIT, SEQUENCE=btp;
REPLACE, ELEMENT=PI.SMH42.btp, BY=PI.SMH42;
ENDEDIT;

SEQEDIT, SEQUENCE=bt2btp;
REPLACE, ELEMENT=PI.SMH42.btp, BY=PI.SMH42;
ENDEDIT;

EXTRACT, SEQUENCE=psb2, FROM=PSB2.START, TO=BR2.BT_START, NEWNAME=psb2_ext;

psbbt2btp: SEQUENCE, refer=ENTRY, L  = lpsbext + lbt2 + lbtp;
    psb2_ext        , AT =  0.0000000000 ;
	bt2          	, AT =  lpsbext ;
    btp          	, AT =  lpsbext + lbt2;
ENDSEQUENCE;

! Matrix to correct for TL reference frame shift
lm1: MATRIX, L=0,  kick1=-xtl, kick2=-pxtl, rm26=-pxtl/(beam->beta), rm51=pxtl/(beam->beta);

SEQEDIT, SEQUENCE = psbbt2btp;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR2.BT_START;
FLATTEN;
ENDEDIT;

! Ensure the kick response is OFF
kBE2KFA14L1 := kBE2KFA14L1REF;

 set, format="22.10e";
use, sequence= psbbt2btp;  
option, -warn;
save, sequence=psbbt2btp, beam, file='jmad/psbbt2btp_tof.jmad';
option, warn;

 set, format="22.10e";
use, sequence= bt2btp; 
option, -warn;
save, sequence=bt2btp, beam, file='jmad/bt2btp_tof.jmad';
option, warn;

 set, format="22.10e";
use, sequence= btp; 
option, -warn;
save, sequence=btp, beam, file='jmad/btp_tof.jmad';
option, warn;

/***************************************
* Copy intial conditions to jmad folder
***************************************/

system, "cp psb2_start_tof.inp jmad";
system, "cp bt2_start_tof.inp jmad";
system, "cp btp2_start_tof.inp jmad";

};

/*************************************
* Cleaning up
*************************************/

if (stitchps == 1){return;};
else {
system, "rm bt_repo";
system, "rm btp_repo";
system, "rm -rf psb_repo || rm psb_repo";
stop;};
