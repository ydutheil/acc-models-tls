/*****************************************************
 * MADX optics file for FTN to nTOF 
 * It also produces stitched moldel from PS extraction
 *
 * F.Velotti
 * Results compared with measurements (M. Fraser)
 *****************************************************/
 title, 'FTN/nTOF optics. Protons - 20 GeV/c';

 option, echo;
 option, RBARC=FALSE;


/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";
system, "rm ./jmad/*.inp";
system, "rm ./jmad/*.seq";

/***************************************
* Load needed repos
***************************************/

system,"[ -d /afs/cern.ch/eng/acc-models/ps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/ps/2021 ps_repo";
system,"[ ! -e ps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-ps -b 2021 ps_repo";

system, "ln -fns ps_repo/scenarios/tof/3_extraction ps_tof_repo";

system, "ln -fns ./../../tt2 tt2_repo";
system, "ln -fns ./../line ftn_line";

system, "ln -fns ./../../ps_ext_elements ps_extr_repo";

/*******************************************************************************
 * Beam
 * momentum 20.32 GeV/c from Beam Doc
 *
 *******************************************************************************/
Beam, particle=PROTON,pc=20.32, exn=(21.0E-6), eyn=(9.0E-6), sige=1.6E-3;
BRHO      := BEAM->PC * 3.3356;

/*****************************************************************************
 * TT2
 * NB! The order of the .ele .str and .seq files matter.
 *
 * The nTOF files have the same strength as the LHC proton files
 *****************************************************************************/
 option, -echo;
 call, file = "tt2_repo/tt2.ele";
 call, file = "ftn_line/tt2_ntof_20.str";
 call, file = "tt2_repo/tt2.seq";
 call, file = "tt2_repo/tt2.dbx";
 option, echo;


/*****************************************************************************
 * FTN - nTOF
 *****************************************************************************/
call, file = "./ftn_line/ftn_20_ntof.ele";
call, file = "./ftn_line/ftn_20_ntof.str";
call, file = "./ftn_line/ftn_20_ntof.seq";
call, file = "./ftn_line/ftn_20_ntof_xy.dbx";

/*****************************************************************************
 * build up the geometry of the beam lines
 *****************************************************************************/
tt2ftn: sequence, refer=ENTRY, l = 397.91003;
  tt2a                  , at =        0;
  tt2b                  , at = 136.3114;
  tt2c                  , at = 249.9449;
  ftn                   , at = 304.6954;
endsequence;

!save, sequence=ftn, file=ftn.save;


/*****************************************************************************
 * Load initial twiss parameters which are then used for FTN optics
 *****************************************************************************/
call, file = "./tt2_ntof_from_stitched_kickers.inp";


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/

X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

exec, set_ini_conditions();


set, format="22.10e";

use, sequence=tt2ftn;

/*************************************
 * Find the initial angle of the beam.
 * The angle is: a1=2.63876699
 *************************************
 a1=2.63876699;
 match, use_macro;
    vary, name=a1,        step=0.000000000001;

    use_macro,name=sur1;

    constraint, expr=table(survey,ROTPOINT,Z)=1595.95632; ! ROTPOINT is a marker of the target. See .seq file
    constraint, expr=table(survey,ROTPOINT,X)=2198.23574;
    constraint, expr=table(survey,ROTPOINT,Y)=2434.22744;
    lmdif, calls=1000, tolerance=6E-7;
 endmatch;

 exec, sur1;

 !write, table=survey;
 !value, a1;
 !value, table(survey,QDE380,Z);
 !save, sequence=ftn, file=ftn_sequence.txt;
 *************************************/

/*******************************************************************************
 * Optics of FTN using initial conditions from stitched model
 *******************************************************************************/

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file="twiss_tt2_ftn.tfs";

l_tt2ftn = table(twiss, FTN.ENDFTN, S);

/*****************************************************************************
 Calculate live initial condition for KR or any other changes in the ring
  - For now only macro to evaluate changes in KFA71
  - KFA71 kick -> 0 for nominal settings
  - The settings to KFA71 given are to be considered as deviation from nominal
*****************************************************************************/

call, file = "ps_extr_repo/macros_ps_ext.madx";
call, file = "./load_tof_extraction.madx";

! It needs as input a KFA71 delta kick (absolute value in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! if "sign" (second argument) = 1, positive kick, negative otherwise

exec, calculate_extraction(0, 1, twiss_ps_ftn_stitched.tfs);

ex_g = 21e-6 / beam->gamma;
ey_g = 9e-6 / beam->gamma;
dpp = 1.6e-3;

mvar1:= sqrt(table(twiss, betx) * ex_g + (table(twiss, dx) * dpp)^2) * 1e3;
mvar2 := sqrt(table(twiss, bety) * ey_g + (table(twiss, dy) * dpp)^2) * 1e3;

! Beam size at the target
value, table(twiss,FTN.ROTPOINT,mvar1),
       table(twiss,FTN.ROTPOINT,mvar2);

exec, calculate_full_stitched(twiss_tt2_ftn.tfs, twiss_ps_tt2_ftn_complete.tfs, tt2ftn);

! Produces twiss file with one single sequence using change of reference
! It uses the PS reference system in the PS and it switches to the TL one 
! just after extraction
exec, calculate_stitched_absolute_trajecotry(twiss_ps_tt2_ftn_nom.tfs, tt2ftn);

! Save sequence for JMAD
exec, save_stitched_sequence(ps_tt2ftn, ftn);

/************************************
* Cleaning up
************************************/

system, "rm -rf ps_repo || rm ps_repo";
system, "rm ps_tof_repo";
system, "rm tt2_repo";
system, "rm ftn_line";
system, "rm ps_extr_repo";
stop;
