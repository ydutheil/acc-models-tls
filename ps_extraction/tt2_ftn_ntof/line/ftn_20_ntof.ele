!------------------------------------------------------------------
! FTN elements definitions
! F.M. Velotti, modification of PC => https://edms.cern.ch/ui/file/1870629/1.0/PS-R-EC-0006-10-00.pdf 
!------------------------------------------------------------------

! magnets are called  QFO and  QDE
! magnets of string are called *.F
! bending magnet polarities as in PS ring:
! positive angles mean bending to the right or upwards


! QUADRUPOLES ******************************************************

 ftn.QDE380   :   QDE240.F;

 QDENEW.F :   QUADRUPOLE,   L= 0.8200,    TYPE=D,   K1:= KQDENEW.F;
 QFONEW.F :   QUADRUPOLE,   L= 0.8000,    TYPE=FS,  K1:= KQFONEW.F;
 ftn.qfo415   :   qfonew.f;
 ftn.qde430   :   qdenew.f;
 ftn.qfo435   :   qfonew.f;
 ftn.qde450   :   qdenew.f;
 ftn.qfo465   :   qfonew.f;
 ftn.qde480   :   qdenew.f;


 ! BENDING MAGNETS ***************************************************
 ! PC modifications after LS2 => 3 converters instead of 2 for the following 6 magnets

 angle_bhz = 2.00*2*pi/360;
 ! These are the 3 independent PCs
 bhz403s = -angle_bhz;
 bhz409s = angle_bhz;
 bhz459s = angle_bhz;

 BHZ40   :   RBEND,   L= 2.5000;
 ftn.bhz403   :   BHZ40, ANGLE := bhz403s;
 ftn.bhz406   :   BHZ40, ANGLE := bhz403s;

 ftn.bhz409   :   BHZ40, ANGLE := -1 * bhz409s;
 ftn.bhz456   :   BHZ40, ANGLE := bhz409s;

 ftn.bhz459   :   BHZ40, ANGLE := bhz459s;
 ftn.bhz462   :   BHZ40, ANGLE := bhz459s;



 ! CORRECTORS
 HCORR    :   HKICKER, L= 1.0000, KICK:=AHK1;
 VCORR    :   VKICKER, L= 1.0000, KICK:=AVK1;
 ftn.dhz436   :   hcorr;
 ftn.dvt451   :   vcorr;


 ! OTHER ELEMENTS
 FTNBTV   :   MONITOR, L= 0.2000; ! TV light monitor
 ftn.btv414   :   ftnbtv;
 ftn.btv454   :   ftnbtv;
 ftn.btv484   :   ftnbtv;

 BSTOPP   :   MONITOR, L= 1.4570; ! Beam stopper
 ftn.stp426   :   bstopp;
 ftn.stp428   :   bstopp;

 ftn.bctdc468   :   MONITOR, L= 0.1495; ! Beam transformer
 ftn.uwb474   :   MONITOR, L= 0.4000; ! PU

 ftn.bhz377   :  marker;
 ftn.bhz378   :  marker;


 ! VACUUM ELEMENTS: CHAMBERS, PUMPS AND VALVES
 ftn.vc377        :  marker;
 ftn.vpi31        :  drift, L=0.893;
 ftn.vvs411       :  marker;
 ftn.vpi413       :  drift, L=1.965;
 ftn.vc414        :  drift, L=0.191;
 ftn.vc416        :  drift, L=2.6505+5.352; ! Really two vacuum chambers
 ftn.vpi423       :  drift, L=0.943;
 ftn.vc427        :  marker;
 ftn.VC429        :  marker;
 ftn.VC431        :  drift, L=5.952;
 ftn.VC433        :  drift, L=0.082;
 ftn.VC434        :  drift, L=5.93;
 ftn.VC439        :  drift, L=2.307;
 ftn.vpg443       :  drift, L=1.414;
 ftn.vc446        :  drift, L=5.383;
 ftn.vc451        :  drift, L=0.102;
 ftn.vc452        :  drift, L=3.932;
 ftn.vpi453       :  drift, L=2.002;
 ftn.vc454        :  drift, L=0.190;
 ftn.vc466        :  drift, L=0.102;
 ftn.vc469        :  drift, L=0.102;
 ftn.vc477        :  drift, L=1.644;
 ftn.vc482        :  drift, L=0.102;
 ftn.vc485        :  drift, L=0.102;
 ftn.vc486        :  drift, L=2.240;
 return;

