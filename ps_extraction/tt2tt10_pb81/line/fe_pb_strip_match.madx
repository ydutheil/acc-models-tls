
/*******************************************************************************
 *******************************************************************************
 * MADX file for TT2/TT10 optics calculations
 * This file is for LHC Ions with a momentum per nucleon of 6.75 GeV/c/u
 *
 * The ions extracted from the PS have a charge state 54+
 * and have the same magnetic rigidity as a proton beam at 26 GeV/c
 * The ions are stripped in TT2 to 82+
 *
 *
 * Execute with:  >madx < fe_pb_strip.madx
 *
 *
 * This MADX file compute the optics for the ions, by dividing the TT2-TT10 line in two parts.
 *  1) Twiss computation from the beginning of TT2 to the stripper (STRN : marker;).
 *  2) The Coulomb scattering at the stripping foil and its effect on the Twiss parameter
 *     are taken into account. The modified Twiss parameters (after the stripping foil) are used
 *     as initial conditions for the second part of the line, from STRN to the end of TT10.
 * (G.Arduini, E.Benedetto, Oct 2007)
 *******************************************************************************
 *******************************************************************************/


 option, echo;
!option, double;
 option, RBARC=FALSE;
!assign, echo="echo.prt";



/*******************************************************************************
 * TITLE
 *******************************************************************************/
 title, 'TT2/TT10 optics. LHC Ion beam 54+/82+, 6.75 GeV/c/u ~ 26 GeV/c proton beam';



/*******************************************************************************
 * TT2
 * NB! The order of the .ele .str and .seq files matter.
 *     The reason is a >feature< of MADX
 *******************************************************************************/
 option, -echo;
 call, file = '/afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2014/elements/tt2.ele';
 call, file = '/afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2014/strength/tt2_fe_26.str';
 call, file = '/afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2014/sequence/tt2.seq';
 call, file = '/afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2014/aperture/tt2.dbx';
 option, echo;



/*******************************************************************************
 * TT10
 * NB! The order of the .ele .str and .seq files matter.
 *     The reason is a >feature< of MADX
 *******************************************************************************/
 option, -echo;
 call, file = '/afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2014/elements/tt10.ele';
 call, file = '/afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2014/strength/tt10_fe_26_Q20.str';
 call, file = '/afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2014/sequence/tt10.seq';
 call, file = '/afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2014/aperture/tt10.dbx';
 option, echo;




/*******************************************************************************
 * SPS
 *******************************************************************************/

 option, -echo;
 call, file = '/afs/cern.ch/eng/sps/2014
               /elements/sps.ele';
 call, file = '/afs/cern.ch/eng/sps/2014
               /aperture/aperturedb_1.dbx';
 call, file = '/afs/cern.ch/eng/sps/2014
               /aperture/aperturedb_2.dbx';
 call, file = '/afs/cern.ch/eng/sps/2014
               /sequence/sps.seq';
 call, file = '/afs/cern.ch/eng/sps/2014
               /aperture/aperturedb_3.dbx';
 call, file = '/afs/cern.ch/eng/sps/2014
               /strength/ft_noqs_ext.str';
 call, file = '/afs/cern.ch/eng/sps/2014
               /strength/elements.str';
 option, echo;



/*******************************************************************************
 * Junction between TT10 and SPS
 *******************************************************************************/

 SEQEDIT, SEQUENCE=SPS;
 ENDTT10 : MARKER;
 install, element = ENDTT10, at = -0.1375, from = BPH.12008;
 ENDEDIT;

 SEQEDIT, SEQUENCE=SPS;
 ENDVKNV : MARKER;
 install, element = ENDVKNV, at = 10.3015+0.1755, from = BPCE.11833;
 ENDEDIT;

 SEQEDIT, SEQUENCE=SPS;
 flatten ; cycle, start=  ENDVKNV;
 ENDEDIT;



/*******************************************************************************
 * Build up the geometry of the beam lines and select a line
 *******************************************************************************/

tt2:        sequence, refer=ENTRY, l = 304.6954;
           tt2a                  , at =        0;
           tt2b                  , at = 136.3114;
           tt2c                  , at = 249.9449;
endsequence;

tt2tt10:    sequence, refer=ENTRY, l = 1164.8409;
           tt2a                  , at =         0;
           tt2b                  , at =  136.3114;
           tt2c                  , at =  249.9449;
           tt10                  , at =  304.6954;
endsequence;

tt2tt10sps: sequence, refer=ENTRY, l = 8076.3447;
           tt2a                  , at =         0;
           tt2b                  , at =  136.3114;
           tt2c                  , at =  249.9449;
           tt10                  , at =  304.6954;
           sps                   , at = 1164.8409;
endsequence;



/*******************************************************************************
 * Set initial twiss parameters. Start of TT2
 *******************************************************************************/
 call, file = '/afs/cern.ch/eng/ps/cps/TransLines/PS-SPS/2014/inp/tt2_fe_26.inp';


/*******************************************************************************
 * set initial position and angle (x,px)
 * E.g. :
 * x0 := 0.0;
 * px0:= 0.0;
 *******************************************************************************/



/*******************************************************************************
 * store initial parameters in memory block
 *******************************************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;



/*******************************************************************************
 * save a sequence
 *******************************************************************************/
 !save, sequence=tt2, file=tt2.save;



/*******************************************************************************
 * maketwiss macro
 *******************************************************************************/
 maketwiss : macro={
!                   select,flag=ptc_twiss, column = name,keyword, s,y,py;
                    ptc_create_universe;
                    ptc_create_layout,model=2,method=6,nst=5,time=true,exact;
                    ptc_twiss,table=ptc_twiss,BETA0=INITBETA0,DELTAP=0,icase=5,no=1; ! , file="twiss"
                    ptc_end;

                    value,  KQDE163, KQDE207, KQFO165, KQDE180, KQFO205, KQDE210, KQDE213, KQFO215, KQDE217;
                    value,  KQIID1001,KQIIF1002,KQIID1003,KQIF1004,KQID1005;

                   }



/*******************************************************************************
 *******************************************************************************
 *
 * MATCHING  before the stripper
 *
 *******************************************************************************
 *******************************************************************************/

 Beam, particle=PROTON,pc=26,exn=1.4E-6*4.0,eyn=1.4E-6*4.0;
 use, sequence=tt2, range=#s/strn;



/***************************************************
 * TWISS PTC. Start of TT2
 ***************************************************/
 exec, maketwiss;


/***************************************************
 * MATCHING  before the stripper
 ***************************************************/
 match,use_macro;
!   vary, name=KQDE163, step = 0.0001;
    vary, name=KQDE207, step = 0.0001;
    vary, name=KQFO165, step = 0.0001;
    vary, name=KQDE180, step = 0.0001;
    vary, name=KQFO205, step = 0.0001;

    use_macro,name=maketwiss;
    constraint, expr=table(ptc_twiss, STRN, beta11)    = 40   ;
    constraint, expr=table(ptc_twiss, STRN, beta22)    = 40   ;
    constraint, expr=table(ptc_twiss, STRN, alfa11)*10 =  0*10;
    constraint, expr=table(ptc_twiss, STRN, alfa22)*10 =  0*10;

!   constraint, expr=KQDE163*10000 < 0;
    constraint, expr=KQDE207*10000 < 0;
    constraint, expr=KQFO165*10000 > 0;
    constraint, expr=KQDE180*10000 < 0;
    constraint, expr=KQFO205*10000 > 0;



    lmdif, calls = 5000, tolerance = 1.0E-21;
!   simplex, calls = 1000, tolerance = 1.0E-25;
!   lmdif, calls = 50, tolerance = 1.0E-21;
!   simplex, calls = 10, tolerance = 1.0E-25;
 endmatch;






/*******************************************************************************
 *******************************************************************************
 *
 * Evaluation of the beam parameters after the stripper.
 *
 *******************************************************************************
 *******************************************************************************/

!   In order to estimate the emittance after the stripper, we need to evaluate:
!    1) the blow-up due to the stripper
!    2) the twiss parameters at the start of the stripper

 exnbefstrip  = (beam->exn)/4;
 eynbefstrip  = (beam->eyn)/4;

 exbefstrip = exnbefstrip/((beam->beta)*(beam->gamma));  ! emittance before the stripper - HOR
 eybefstrip = eynbefstrip/((beam->beta)*(beam->gamma));  ! emittance before the stripper - VER

 dpp    = 5.5e-4; ! energy spread
 ZSCAT  = 1;
 L      = 0.8e-3; ! stripper thickness [m]
 csi0   = 8.9e-2; ! X0 = Radiation length of aluminium (89mm)
 thetascatt_rms = 13.6 * ZSCAT/((beam->beta)*(beam->pc)*1000) * sqrt(L/csi0) * (1+0.038*log(L/csi0));

 value, thetascatt_rms;! Scattering angle, caused by the stripper
 value, beam->beta, beam->pc, sqrt(L/csi0);

 delta_epsx     = 0.5*table(ptc_twiss, STRN, beta11)*thetascatt_rms^2; ! emittance blow-up- HOR
 delta_epsy     = 0.5*table(ptc_twiss, STRN, beta22)*thetascatt_rms^2; ! emittance blow-up- VER

 value, delta_epsx, delta_epsy;


 exnafterstrip = exnbefstrip+delta_epsx*(beam->beta)*(beam->gamma); ! norm. emittance after stripper - HOR
 eynafterstrip = eynbefstrip+delta_epsy*(beam->beta)*(beam->gamma); ! norm. emittance after stripper - VER

 exafterstrip = exnafterstrip/((beam->beta)*(beam->gamma));  ! emittance after the stripper - HOR
 eyafterstrip = eynafterstrip/((beam->beta)*(beam->gamma));  ! emittance after the stripper - VER



 /***************************************************
  * store optical parameters @ STRN in memory block: BEGTT2
  ***************************************************/
 BEGTT2: BETA0,
  BETX=table(ptc_twiss,BEGTT2A,beta11),
  ALFX=table(ptc_twiss,BEGTT2A,alfa11),
  MUX=table(ptc_twiss,BEGTT2A,mu1),
  BETY=table(ptc_twiss,BEGTT2A,beta22),
  ALFY=table(ptc_twiss,BEGTT2A,alfa22),
  MUY=table(ptc_twiss,BEGTT2A,mu2),
  X=table(ptc_twiss,BEGTT2A,x),
  PX=table(ptc_twiss,BEGTT2A,px),
  Y=table(ptc_twiss,BEGTT2A,y),
  PY=table(ptc_twiss,BEGTT2A,py),
  T=0,
  PT=0,
  DX=table(ptc_twiss,BEGTT2A,disp1),
  DPX=table(ptc_twiss,BEGTT2A,disp2),
  DY=table(ptc_twiss,BEGTT2A,disp3),
  DPY=table(ptc_twiss,BEGTT2A,disp4);


/***************************************************
 * store optical parameters @ STRN in memory block: STRIPEXIT
 ***************************************************/
STRIPEXIT: BETA0,
  BETX=(table(ptc_twiss,STRN,beta11)*exbefstrip+L^2/3*thetascatt_rms^2)/(exbefstrip+delta_epsx),
  ALFX=(table(ptc_twiss,STRN,alfa11)*exbefstrip-0.5*L*thetascatt_rms^2)/(exbefstrip+delta_epsx),
  MUX=table(ptc_twiss,STRN,mu1),
  BETY=(table(ptc_twiss,STRN,beta22)*eybefstrip+L^2/3*thetascatt_rms^2)/(eybefstrip+delta_epsy),
  ALFY=(table(ptc_twiss,STRN,alfa22)*eybefstrip-0.5*L*thetascatt_rms^2)/(eybefstrip+delta_epsy),
  MUY=table(ptc_twiss,STRN,mu2),
  X=table(ptc_twiss,STRN,x),
  PX=table(ptc_twiss,STRN,px),
  Y=table(ptc_twiss,STRN,y),
  PY=table(ptc_twiss,STRN,py),
  T=0,
  PT=0,
  DX=table(ptc_twiss,STRN,disp1),
  DPX=table(ptc_twiss,STRN,disp2),
  DY=table(ptc_twiss,STRN,disp3),
  DPY=table(ptc_twiss,STRN,disp4);

 value, STRIPEXIT->DX, STRIPEXIT->DPX;
 value, STRIPEXIT->X, STRIPEXIT->PX;
 value, (table(ptc_twiss,STRN,beta11)*exbefstrip+L^2/3*thetascatt_rms^2)/(exbefstrip+delta_epsx);
 value, (table(ptc_twiss,STRN,alfa11)*exbefstrip-0.5*L*thetascatt_rms^2)/(exbefstrip+delta_epsx);
 value, (table(ptc_twiss,STRN,beta22)*eybefstrip+L^2/3*thetascatt_rms^2)/(eybefstrip+delta_epsy);
 value, (table(ptc_twiss,STRN,alfa22)*eybefstrip-0.5*L*thetascatt_rms^2)/(eybefstrip+delta_epsy);

 show, STRIPEXIT;
 value,  table(ptc_twiss,STRN,disp1),
 value,  table(ptc_twiss,STRN,disp2),
 value,  table(ptc_twiss,STRN,disp3),
 value,  table(ptc_twiss,STRN,disp4);








/*******************************************************************************
 *******************************************************************************
 *
 * MATCHING  after the stripper
 *
 *******************************************************************************
 *******************************************************************************/

/***************************************************
 * Set up beam parameters after the stripper
 ***************************************************/
 Beam, particle=PROTON,pc=26,exn=exnafterstrip*4,eyn=eynafterstrip*4;
 use,  sequence=tt2tt10, range=strn/#e;
!save, sequence=tt2tt10, file=tt2tt10.save;



/***************************************************
 * Set initial twiss parameters. After the stripper in TT2
 ***************************************************/
 BETX0         = STRIPEXIT->betx;
 BETY0         = STRIPEXIT->bety;
 ALFX0         = STRIPEXIT->alfx;
 ALFY0         = STRIPEXIT->alfy;
 DX0           = STRIPEXIT->dx;
 DPX0          = STRIPEXIT->dpx;
 DY0           = STRIPEXIT->dy;
 DPY0          = STRIPEXIT->dpy;

 value, betx0,bety0,alfx0,alfy0,dx0,dy0,dpx0,dpy0,exbefstrip,eybefstrip,beam->ex,beam->ey;
 show, STRIPEXIT;
 show, beam;



/***************************************************
 * store initial parameters in memory block
 ***************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;




/***************************************************
 * TWISS PTC. STRN to END
 ***************************************************/
 exec, maketwiss;


/****************************************************
 * MATCHING  after the stripper
 ****************************************************/
! KQFO375 =0;
! KQIID1003 =0;

 match,use_macro;
    vary, name=KQDE210    , step = 0.0001;
!   vary, name=KQDE213    , step = 0.0001;
    vary, name=KQFO215    , step = 0.0001;
!   vary, name=KQDE217    , step = 0.0001;
    vary, name=KQDE220.f  , step = 0.0001;
    vary, name=KQFO225.f  , step = 0.0001;
    vary, name=KQFO375    , step = 0.0001;
    vary, name=KQIID1001  , step = 0.0001;
    vary, name=KQIIF1002  , step = 0.0001;
    vary, name=KQIID1003  , step = 0.0001;
    vary, name=KQIF1004   , step = 0.0001;
    vary, name=KQID1005   , step = 0.0001;
    vary, name=KQIF1006   , step = 0.0001;
    vary, name=KQID1007M  , step = 0.0001;
    vary, name=KQIF1008M  , step = 0.0001;
    vary, name=KQID1011M  , step = 0.0001;
    vary, name=KQIF1012M  , step = 0.0001;



!  vary, name=qiid1001.f->k1, step = 0.0001;

    use_macro,name=maketwiss;
    constraint, expr=table(ptc_twiss, TT10$END, beta11)    = 64.27057   ;
    constraint, expr=table(ptc_twiss, TT10$END, beta22)    = 54.94536   ;
    constraint, expr=table(ptc_twiss, TT10$END, alfa11)*10 =  1.33233*10;
    constraint, expr=table(ptc_twiss, TT10$END, alfa22)*10 = -1.16995*10;
    constraint, expr=table(ptc_twiss, TT10$END, disp1 )*10 = -0.77754*10;
    constraint, expr=table(ptc_twiss, TT10$END, disp2 )*10 =  0.01876*10;
    constraint, expr=table(ptc_twiss, TT10$END, disp3 )*10 =  0.00000*10;
    constraint, expr=table(ptc_twiss, TT10$END, disp4 )*10 =  0.00000*10;

    constraint, expr=KQDE210   *10000 < 0;
    constraint, expr=KQDE210   *10000 > -0.195515147411*10000;
!   constraint, expr=KQDE213   *10000 < 0;
    constraint, expr=KQFO215   *10000 > 0;
    constraint, expr=KQFO215   *10000 <  0.223841782480*10000;
!   constraint, expr=KQDE217   *10000 < 0;
    constraint, expr=KQDE220.f *10000 < 0;
    constraint, expr=KQDE220.f *10000 > -0.173897627456*10000;
    constraint, expr=KQFO225.f *10000 > 0;
    constraint, expr=KQFO225.f *10000 <  0.173360516894*10000;
    constraint, expr=KQFO375   *10000 > 0;
    constraint, expr=KQFO375   *10000 <  0.19*10000;
    constraint, expr=KQIID1001 *10000 < 0;
    constraint, expr=KQIIF1002 *10000 > 0;
    constraint, expr=KQIIF1002 *10000 <  0.137934711538*10000;
    constraint, expr=KQIID1003 *10000 < 0;
    constraint, expr=KQIID1003 *10000 > -0.0995*10000;
    constraint, expr=KQIF1004  *10000 > 0;
    constraint, expr=KQIF1004  *10000 <  0.106*10000; !  0.120874587015*10000;
    constraint, expr=KQID1005  *10000 < 0;
    constraint, expr=KQID1005  *10000 > -0.106*10000; ! -0.120874587015*10000;
    constraint, expr=KQIF1006  *10000 > 0;
    constraint, expr=KQIF1006  *10000 <  0.110*10000; !  0.120874587015*10000;
    constraint, expr=KQID1007M *10000 < 0;
    constraint, expr=KQID1007M *10000 > -0.106*10000; ! -0.120874587015*10000;
    constraint, expr=KQIF1008M *10000 > 0;
    constraint, expr=KQIF1008M *10000 <  0.106*10000; !  0.120874587015*10000;
    constraint, expr=KQID1011M *10000 < 0;
    constraint, expr=KQID1011M *10000 > -0.106*10000; ! -0.120874587015*10000;
    constraint, expr=KQIF1012M *10000 > 0;
    constraint, expr=KQIF1012M *10000 <  0.106*10000; !  0.120874587015*10000;


    jacobian,  calls= 25, tolerance=1.0E-15, bisec=9;
    lmdif, calls = 5000, tolerance = 1.0E-21;
!   simplex, calls = 100, tolerance = 1.0E-25;
!   jacobian,  calls= 25, tolerance=1.0E-15, bisec=9;
!   migrad , calls = 5000 , tolerance = 1.d-5;

 endmatch;

 /**************************************************
  NAME         DISP1     DISP2   ALFA11    BETA11     DISP3     DISP4    ALFA22    BETA22
  TT10$END  -0.77754   0.01876  1.33233  64.27057   0.00000   0.00000  -1.16995  54.94536
 ****************************************************/




/*******************************************************************************
 *******************************************************************************
 **  TT2[#S] - TT2[STRN]
 **  From the start of TT2 to the stripper
 *******************************************************************************
 *******************************************************************************/
 Beam, particle=PROTON,pc=26,exn=exnbefstrip*4.0,eyn=eynbefstrip*4.0,sige=1.07e-3;
 show, beam;


/***************************************************
 * Set initial twiss parameters. Start of TT2
 ***************************************************/
 BETX0         = BEGTT2->betx;
 ALFX0         = BEGTT2->alfx;
 MUX0          = BEGTT2->mux;
 BETY0         = BEGTT2->bety;
 ALFY0         = BEGTT2->alfy;
 MUY0          = BEGTT2->muy;
 X0            = BEGTT2->x;
 PX0           = BEGTT2->px;
 Y0            = BEGTT2->y;
 PY0           = BEGTT2->py;
 T0            = BEGTT2->t;
 PT0           = BEGTT2->pt;
 DX0           = BEGTT2->dx;
 DPX0          = BEGTT2->dpx;
 DY0           = BEGTT2->dy;
 DPY0          = BEGTT2->dpy;

 show, begtt2;



/***************************************************
 * store initial parameters in memory block
 ***************************************************/
 INITBETA0: BETA0,
   BETX=BETX0,
   ALFX=ALFX0,
   MUX=MUX0,
   BETY=BETY0,
   ALFY=ALFY0,
   MUY=MUY0,
   X=X0,
   PX=PX0,
   Y=Y0,
   PY=PY0,
   T=T0,
   PT=PT0,
   DX=DX0,
   DPX=DPX0,
   DY=DY0,
   DPY=DPY0;



/***************************************************
 * TWISS @ From start of TT2
 ***************************************************/

 use, sequence=tt2, range=#s/strn;
 exec, maketwiss;

 ns = 2;
 nt = 3;
 dimxc := sqrt( nt^2*table(ptc_twiss,betx)*exbefstrip + ns^2*(beam->sige*table(ptc_twiss,dx))^2 );
 dimyc := sqrt( nt^2*table(ptc_twiss,bety)*eybefstrip + ns^2*(beam->sige*table(ptc_twiss,dy))^2 );


!------------------------ write ptc_twiss table -----------------
 set, format="22.10e";

 select,flag=ptc_twiss, clear;
 select,flag=ptc_twiss, range=#S/#E;
 select,flag=ptc_twiss, column=name,s,x,px,disp1,disp2,alfa11,beta11,mu1,y,py,disp3,disp4,alfa22,beta22,mu2,dimxc,dimyc,APERTYPE,APER_1,APER_2,APER_3,APER_4;

 write,table=ptc_twiss, file="strip1.out";
!----------------------------------------------------------------

!write,table=ptc_twiss;


 use, sequence=tt2, range=#s/strn;
 exec, maketwiss;

 ns = 2;
 nt = 3;
 dimxc := sqrt( nt^2*table(ptc_twiss,betx)*exbefstrip + ns^2*(beam->sige*table(ptc_twiss,dx))^2 );
 dimyc := sqrt( nt^2*table(ptc_twiss,bety)*eybefstrip + ns^2*(beam->sige*table(ptc_twiss,dy))^2 );

!------------------------ write ptc_twiss table -----------------
 set, format="22.10e";

 select,flag=ptc_twiss, clear;
 select,flag=ptc_twiss, range=#S/#E;
 select,flag=ptc_twiss, column=name,s,x,px,disp1,disp2,alfa11,beta11,mu1,y,py,disp3,disp4,alfa22,beta22,mu2,dimxc,dimyc,APERTYPE,APER_1,APER_2,APER_3,APER_4;

 write,table=ptc_twiss, file="strip1.out";
!----------------------------------------------------------------

!write,table=ptc_twiss;





/*******************************************************************************
 *******************************************************************************
 **  TT2[STRN] - TT10
 **  From the the stripper in TT2 to SPS
 *******************************************************************************
 *******************************************************************************/

/*******************************************************************************
 * Set up beam parameters after the stripper
 *******************************************************************************/
 Beam, particle=PROTON,pc=26,exn=exnafterstrip*4,eyn=eynafterstrip*4,sige=1.07e-3;
 use, sequence=tt2tt10, range=strn/#e;



/*******************************************************************************
 * Set initial twiss parameters. At the stripper in TT2
 *******************************************************************************/
 BETX0         = STRIPEXIT->betx;
 ALFX0         = STRIPEXIT->alfx;
 MUX0          = STRIPEXIT->mux;
 BETY0         = STRIPEXIT->bety;
 ALFY0         = STRIPEXIT->alfy;
 MUY0          = STRIPEXIT->muy;
 X0            = STRIPEXIT->x;
 PX0           = STRIPEXIT->px;
 Y0            = STRIPEXIT->y;
 PY0           = STRIPEXIT->py;
 T0            = STRIPEXIT->t;
 PT0           = STRIPEXIT->pt;
 DX0           = STRIPEXIT->dx;
 DPX0          = STRIPEXIT->dpx;
 DY0           = STRIPEXIT->dy;
 DPY0          = STRIPEXIT->dpy;


 value, betx0,bety0,alfx0,alfy0,dx0,dy0,dpx0,dpy0,mux0,muy0,exbefstrip,eybefstrip,beam->ex,beam->ey;
 show, STRIPEXIT;
 show, beam;



/*******************************************************************************
 * store initial parameters in memory block
 *******************************************************************************/
INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;


 show, INITBETA0;

/*******************************************************************************
 * TWISS @ From the stripper in TT2 to SPS
 *******************************************************************************/
 use, period=tt2tt10, range=strn/#e;
 exec, maketwiss;

 ns = 2;
 nt = 3;
 dimxc := sqrt( nt^2*table(ptc_twiss,betx)*exafterstrip + ns^2*(beam->sige*table(ptc_twiss,dx))^2 );
 dimyc := sqrt( nt^2*table(ptc_twiss,bety)*eyafterstrip + ns^2*(beam->sige*table(ptc_twiss,dy))^2 );

 exec, maketwiss;

!------------------------ write ptc_twiss table -----------------

 set, format="22.10e";

 select,flag=ptc_twiss, clear;
 select,flag=ptc_twiss, range=#S/#E;
 select,flag=ptc_twiss, column=name,s,x,px,disp1,disp2,alfa11,beta11,mu1,y,py,disp3,disp4,alfa22,beta22,mu2,dimxc,dimyc,APERTYPE,APER_1,APER_2,APER_3,APER_4;

 write,table=ptc_twiss, file="strip2.out";
!----------------------------------------------------------------

!write,table=ptc_twiss;






/*******************************************************************************
 * Write results on
 *******************************************************************************/
 set, format="22.10e";

 assign, echo="result.txt";

 print, text=" ";
 print, text="The optics-function at the exit of the stripper";
 value, STRIPEXIT->betx;
 value, STRIPEXIT->alfx;
 value, STRIPEXIT->mux;
 value, STRIPEXIT->bety;
 value, STRIPEXIT->alfy;
 value, STRIPEXIT->muy;
 value, STRIPEXIT->x;
 value, STRIPEXIT->px;
 value, STRIPEXIT->y;
 value, STRIPEXIT->py;
 value, STRIPEXIT->t;
 value, STRIPEXIT->pt;
 value, STRIPEXIT->dx;
 value, STRIPEXIT->dpx;
 value, STRIPEXIT->dy;
 value, STRIPEXIT->dpy;

 print, text=" ";
 print, text="The normalized emittance and the emittance before and after the stripper";
 value, exnbefstrip ,
        eynbefstrip ;
 value, exbefstrip ,
        eybefstrip ;
 value, exnafterstrip ,
        eynafterstrip ;
 value, exafterstrip ,
        eyafterstrip ;

 print, text=" ";
 print, text="The strengths for the matching";
 value, KQFO105     ;
 value, KQDE120     ;
 value, KQFO135     ;
 value, KQDE150     ;
 value, KQDE163     ;
 value, KQFO165     ;
 value, KQDE180     ;
 value, KQFO205     ;
 value, KQDE207     ;
 value, KQDE210     ;
 value, KQDE213     ;
 value, KQFO215     ;
 value, KQDE217     ;
 value, KQDE220.F   ;
 value, KQFO225.F   ;
 value, KQDE240.F   ;
 value, KQFO375     ;

 value, KQIID1001   ;
 value, KQIIF1002   ;
 value, KQIID1003   ;
 value, KQIF1004    ;
 value, KQID1005    ;
 value, KQIF1006    ;
 value, KQID1007M   ;
 value, KQIF1008M   ;
 value, KQID1011M   ;
 value, KQIF1012M   ;
 value, KQISK1006M  ;

 assign, echo=terminal;

/*******************************************************************************
 * Comments
 *******************************************************************************/
 print, text="The file: echo.prt is on echo.prt";


 stop;

