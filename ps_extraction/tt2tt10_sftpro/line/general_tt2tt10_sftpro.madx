/*****************************************************
 * MADX optics file for SFTPRO transfer (MTE beam)
 * Based on files from E. Benedetto and S. Gilardoni, checked by A. Huschauer
 *
 * F.Velotti
 *****************************************************/
 title, "TT2/TT10 for SFTPRO (MTE) optics. Protons - 14 GeV/c";

 option, echo;
 option, RBARC=FALSE;  ! the length of a rectangular magnet
                       ! is the distance between the polefaces
                       ! and not the arc length

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/*******************************************************************************
 * Beam
 *******************************************************************************/
Beam, particle=PROTON, pc=14;
BRHO      := BEAM->PC * 3.3356;

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt2 tt2_repo";
system, "ln -fns ./../../tt10 tt10_repo";

/*****************************************************************************
 * TT2
 *****************************************************************************/
 option, -echo;
 call, file = "./tt2_repo/tt2.ele";
 call, file = "./tt2_mte_2010.str";
 call, file = "./tt2_repo/tt2.seq";
 call, file = "./tt2_repo/tt2.dbx";

 option, echo;


/*******************************************************************************
 * TT10
 *******************************************************************************/

 call, file = "./tt10_repo/tt10.ele";
 call, file = "./tt10_mte_2010.str";
 call, file = "./tt10_repo/tt10.seq";
 call, file = "./tt10_repo/tt10.dbx";


/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/

 tt2tt10: sequence, refer=ENTRY, l = 1164.8409;
  tt2a                  , at =        0;
  tt2b                  , at = 136.3114;
  tt2c                  , at = 249.9449;
  tt10                  , at = 304.6954;
 endsequence;


/*******************************************************************************
 * set initial twiss parameters
 *******************************************************************************/
! Those should be the correct initial conditions as obtained from the PS
!call, file = "./../stitched/tt2_mte_from_stitched.inp";

! Hystorical conditions and kept only until re-matching done
call, file = "./../stitched/tt2_mte_2010.inp";


INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;


/*******************************************************************************
 * Twiss
 *******************************************************************************/

use, sequence=tt2tt10;

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "./twiss_tt2tt10_sftpro_nom.tfs";

/*******************************************************************
* Saving sequence for JMAD
*******************************************************************/

system, "rm ./jmad/*";
system, "mkdir jmad";

 set, format="22.10e";
option, -warn;
save, sequence = tt2tt10, file="./jmad/tt2_sftpro_savedseq.seq", beam;
option, warn;


/***********************************************
* Save initial parameters to file for TL usage
***********************************************/
assign, echo="./jmad/tt2_sftpro.inp";

betx0 = initbeta0->betx;
bety0 = initbeta0->bety;
alfx0 = initbeta0->alfx;
alfy0 = initbeta0->alfy;
dx0   = initbeta0->dx;
dy0   = initbeta0->dy;
dpx0  = initbeta0->dpx;
dpy0  = initbeta0->dpy;

print, text="/*********************************************************************************";
print, text='Initial conditions from MADX stitched model of PS extraction';
print, text="*********************************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

assign, echo=terminal;
/************************************
* Cleaning up
************************************/
system, "rm tt2_repo";
system, "rm tt10_repo";
stop;