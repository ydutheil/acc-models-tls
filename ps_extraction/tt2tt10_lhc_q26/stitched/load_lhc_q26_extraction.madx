/***********************************************************************
 * MAD-X script to load LHC Q26 extraction from PS up to SPS injection
 **
 ** 07/08/2019
 ** F.Velotti, M. Fraser
 ***********************************************************************/

option, RBARC=FALSE;

/******************************************************************
 * Call lattice files
 ******************************************************************/
option, -warn;
call, file="ps_repo/ps_mu.seq";
call, file="ps_repo/ps_ss.seq";
call, file="ps_lhc_repo/ps_ext_lhc.str";
call, file="ps_extr_repo/fringe_field_fix.madx";
call, file = "ps_extr_repo/ft16.ele";
option, warn;
/******************************************************************
 * Twiss with bump on
 ******************************************************************/

use, sequence=PS;
twiss;

/**********************************************
* Make extraction sequence to get to where there
* is the handover with TT2
**********************************************/

 set, format="22.10e";

! Nominal kick strength for KFA71 for nTOF => assumed the same for AD and LHC
kfa71_nom_kick = -1.15e-03;

ksmh16_error = -29.4e-3;


kPEKFA71 := kfa71_k;
kPIKFA45 := kfa45_kick ;
kPIKFA28 := kfa28_kick ;

SEQEDIT, sequence=PS;
FLATTEN;
ENDEDIT;

use, sequence = PS;

! Cycle sequence and changing starting point

SEQEDIT, sequence=PS;
CYCLE, START=PR.BPM23;
FLATTEN;
ENDEDIT;

use, sequence=PS;
twiss;

start_psej = table(twiss, PR.BPM23, s);
end_psej = table(twiss, PE.BTV16, s);

len_psej = end_psej - start_psej;

value, len_psej;

/*******************************************
* Install septum and consider fringe field
*******************************************/
len_ft16 = 7.701173928;

SEPTUM16E: MARKER;
FT16: sequence, refer=entry, l=len_ft16;
MTV001        ,at=0.0;
SEPTUM16      ,at=0.0;
SEPTUM16E       ,at=2.700072902;
D16STRAY      ,at=2.700072902;
F16SHIM       ,at=4.904893572;
pointR        ,at=7.701173928;
ENDSEQUENCE;

! POINTR is the handover point between PS and TT2 => initial conditions

/***********************************
* PS_EJ sequence definition
***********************************/

EXTRACT, sequence=PS, FROM=PR.BPM23, TO=PE.BTV16, newname=PS_EJ;


PS_EXTRACT: sequence, refer=entry, l=len_psej + len_ft16 ;
PS_EJ,       at=0.0;
FT16,        at=len_psej;
ENDSEQUENCE;

kfa71_k =0;
kfa45_kick=0;
kfa28_kick=0;

USE, sequence=PS;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta, label=bumped, place = PR.BPM23;
twiss, file = "ps_ext_lhc_bump.tfs";


calculate_extraction(delta_kfa71, sign, ring_twiss_file) : macro = {
    
    if(sign == 1){
        kick_s = 1.0;
    }   
    else{
        kick_s = -1.0;
    };

    create,table=trajectory, column=_NAME,S,L, _KEYWORD, BETX,ALFX, x, px, dx, dpx, MUX,BETY,ALFY,Y,DY,PY,DPY,MUY, k1l;
    use, sequence = PS_EXTRACT;
    kfa71_k = kfa71_nom_kick;
    kfa45_kick=0e-4;
    kfa28_kick=0e-4;

    exec, septum16_on();
    twiss, beta0 = bumped;

    /* x_stray_field = table(twiss, SEPTUM16E, X) - 91.6e-3; */
    /* px_stray_field = table(twiss, SEPTUM16E, PX) - 62.6e-3; */
    x_stray_field = table(twiss, SEPTUM16E, X);
    px_stray_field = table(twiss, SEPTUM16E, PX);
    exec, place_stray_field(x_stray_field, px_stray_field);

    twiss, beta0 = bumped, table=twiss_nom;

    value, delta_kfa71;
    kfa71_k = kfa71_nom_kick + delta_kfa71 * kick_s;
    kfa45_kick=0e-4;
    kfa28_kick=0e-4;

    exec, septum16_on();
    twiss, beta0 = bumped;

    exec, place_stray_field(x_stray_field, px_stray_field);

    savebeta,label=initial_cond, place = POINTR;
    select, flag = twiss, clear;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX, X, DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
    twiss, beta0 = bumped;

    len_twiss = table(twiss_nom, tablelength);
    value, len_twiss;

    i = 2;
    option, -info;
    while(i < len_twiss){

        SETVARS, TABLE=twiss_nom, ROW=i;
        x0 = x;
        px0 = px;
        SETVARS, TABLE=twiss, ROW=i;
        x = x - x0;
        px = px - px0;

        fill, table=trajectory;

        i = i + 1;
    };
    option, info;
    write, table=trajectory, file="ring_twiss_file";
    
    betx0 = initial_cond->betx;
    bety0 =  initial_cond->bety;

    alfx0 = initial_cond->alfx;
    alfy0 = initial_cond->alfy;

    dx0 = initial_cond->dx;
    dy0 = initial_cond->dy;

    dpx0 = initial_cond->dpx - initial_cond->px/(beam->beta);
    dpy0 = initial_cond->dpy;

    x0 = x;
    y0 = initial_cond->y;

    px0 = px;
    py0 = initial_cond->py;

    mux0 = initial_cond->mux;
    muy0 = initial_cond->muy;

};


calculate_full_stitched(__file_name_line__, __file_name_stitched__, __sequence_name__) : macro = {

    exec, set_ini_conditions();

    use, sequence=__sequence_name__;
    select, flag=twiss, clear;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26, mvar1, mvar2;
    twiss, beta0=initbeta0, file = "__file_name_line__";

    len_twiss_tl = table(twiss, tablelength);

    i = 2;
    option, -info;
    while(i < len_twiss_tl){

        if(i == 2){
            s0 = s;
            value, s0;
        }
        SETVARS, TABLE=twiss, ROW=i;
        s = s + s0;
        fill, table=trajectory;

        i = i + 1;
    };

    option, info;
    write, table=trajectory, file="__file_name_stitched__";
};


calculate_stitched_absolute_trajecotry(__file_name__, __seq_name__) : macro = {


    use, sequence = PS_EXTRACT;
    kfa71_k = kfa71_nom_kick;
    kfa45_kick=0;
    kfa28_kick=0;

    exec, septum16_on();

    twiss, beta0 = bumped;

    /* x_stray_field = table(twiss, SEPTUM16E, X) - 91.6e-3; */
    /* px_stray_field = table(twiss, SEPTUM16E, PX) - 62.6e-3; */
    x_stray_field = table(twiss, SEPTUM16E, X);
    px_stray_field = table(twiss, SEPTUM16E, PX);

    exec, place_stray_field(x_stray_field, px_stray_field);

    select, flag = twiss, clear;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26, mvar1, mvar2;
    savebeta,label=initial_cond, place = POINTR;
    twiss, beta0 = bumped;

    x_inj  = -1 * table(twiss, POINTR, x);
    px_inj = -1 * table(twiss, POINTR, px);

    value, x_inj, px_inj;

    change_ref: MATRIX, L=0,  kick1 = x_inj, kick2 = px_inj, rm26= px_inj/(beam->beta), rm51 = -px_inj/(beam->beta);

    seqedit, sequence = ps_extract;
        install, element = change_ref, at = 1e-10, from = POINTR;
        flatten;
    endedit;

    use, sequence = ps_extract;
    twiss, beta0 = bumped;
    l_ps_extract = table(twiss, POINTR, S);

    value, l_ps_extract, l___seq_name__, l_ps_extract + l___seq_name__;

    ps___seq_name__: sequence, refer=entry, l = l_ps_extract + l___seq_name__ + 1e-6; ! there seems to be a rounding error...
        ps_extract, at = 0.0;
        __seq_name__, at = 0.0, from=FT16$END;
    endsequence;

    use, sequence = ps___seq_name__;

    exec, septum16_on();
    exec, place_stray_field(x_stray_field, px_stray_field);
    
    select, flag = twiss, clear;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26, mvar1, mvar2;
    twiss, beta0 = bumped, file="__file_name__";


};





