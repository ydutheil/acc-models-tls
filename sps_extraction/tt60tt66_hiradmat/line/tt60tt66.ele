/*******************************************************************************
! TT60-TT66 elements
!
! F.M. Velotti: reorganisation of file structure for new repo format
! Based on C. Hessler files, and modified by J.L. Abelleira in 2015
 *******************************************************************************/

option, -echo;
option, -warn;


REAL CONST l.BCT  = 0.86;
REAL CONST l.BFCT = 0.4820;
REAL CONST l.BCTFI = 0.4830;
REAL CONST l.BPCK = 0.45;
REAL CONST l.BPCL = 0.72;
REAL CONST l.BTV  = 0.45; 
REAL CONST l.BTVNEW  = 0.0; 
REAL CONST l.BTVI = 0.35; 
REAL CONST l.BPM  = 0.25;
REAL CONST l.BPKG = 0.3;

REAL CONST l.MBB  = 6.26;
REAL CONST l.MBBT = 6.26;
REAL CONST l.MBE  = 6.26;
REAL CONST l.MBS  = 3.00;
REAL CONST l.MDSV = 0.70; 
REAL CONST l.MDSH = 0.70;
REAL CONST l.MDLH = 1.40;
REAL CONST l.MDLV = 1.40;
REAL CONST l.MDAV = 1.40;

REAL CONST l.QTLD = 2.99;
REAL CONST l.QTLF = 2.99;

REAL CONST l.BSTL = 0.86; 
REAL CONST l.TED  = 4.3; 


HCORRECTOR : HKICKER;
VCORRECTOR : VKICKER;

//---------------------- HKICKER        ---------------------------------------
CMDSH: HKICKER, L := l.MDSH,	APERTYPE = ELLIPSE, APERTURE = {0.0635,0.0335};// correcting dipole, short deflection
CMDLH: HKICKER, L := l.MDLH,	APERTYPE = ELLIPSE, APERTURE = {0.0605,0.0315};// correcting dipole, long deflection

//---------------------- VKICKER        ---------------------------------------
CMDSV: VKICKER, L := l.MDSV,	APERTYPE = ELLIPSE, APERTURE = {0.0335,0.0635};// correcting dipole, short deflection
CMDLV: VKICKER, L := l.MDLV,	APERTYPE = ELLIPSE, APERTURE = {0.0315,0.0605};// correcting dipole, long deflection
CMDAV: VKICKER, L := l.MDAV,	APERTYPE = ELLIPSE, APERTURE = {0.043,0.0615};// correcting dipole, enlarged aperture

//---------------------- MONITOR        ---------------------------------------

BPCK: MONITOR, L := l.BPCK,	APERTYPE = ELLIPSE, APERTURE = {0.0575,0.0575};// beam position, directional coupler
BPCL: MONITOR, L := l.BPCL,	APERTYPE = ELLIPSE, APERTURE = {0.0575,0.0575};// beam position, directional coupler
BPM : MONITOR, L := l.BPM,	APERTYPE = ELLIPSE, APERTURE = {0.04,0.04};
BPKG: MONITOR, L := l.BPKG,	APERTYPE = ELLIPSE, APERTURE = {0.03,0.03};

//---------------------- INSTRUMENT     ---------------------------------------

BCT : INSTRUMENT, L := l.BCT,	APERTYPE = ELLIPSE, APERTURE = {0.0775,0.0775}; // beam current transformer
BFCT: INSTRUMENT, L := l.BFCT,	APERTYPE = ELLIPSE, APERTURE = {0.080,0.080};//
BCTFI:INSTRUMENT, L := l.BCTFI,	APERTYPE = ELLIPSE, APERTURE = {0.078,0.078};//
BTV : INSTRUMENT, L := l.BTV,	APERTYPE = RECTANGLE, APERTURE = {0.050,0.03425}; // light screen monitor
BTVI: INSTRUMENT, L := l.BTVI,	APERTYPE = RECTELLIPSE, APERTURE={0.03000, 0.03000, 0.03000, 0.03000};
BSTL: INSTRUMENT, L := l.BSTL,	APERTYPE = ELLIPSE, APERTURE = {0.030,0.030}; // Monitor?
BTVNEW: INSTRUMENT, L := l.BTVNEW;
BLM : MARKER;

//---------------------- QUADRUPOLE     ---------------------------------------

QTLD: QUADRUPOLE, L := l.QTLD,	APERTYPE = ELLIPSE, APERTURE = {0.0315,0.0605};// quadrupole, D=80mm
QTLF: QUADRUPOLE, L := l.QTLF,	APERTYPE = ELLIPSE, APERTURE = {0.0605,0.0315};// quadrupole, D=80mm

//---------------------- RBEND          ---------------------------------------

MBB : RBEND, L := l.MBB,	APERTYPE = ELLIPSE, APERTURE = {0.0645,0.02425}; // bending magnet
MBBT: RBEND, L := l.MBBT,	APERTYPE = ELLIPSE, APERTURE = {0.0645,0.02425};// modified MBB, turned
MBE : RBEND, L := l.MBE,	APERTYPE = ELLIPSE, APERTURE = {0.02425,0.0645}; // modified MBB, vertical deflection
MBS : RBEND, L := l.MBS,	APERTYPE = ELLIPSE, APERTURE = {0.0645,0.02465}; // fast pulsed bending magnet
MDSV: RBEND, L := l.MDSV,	APERTYPE = ELLIPSE, APERTURE = {0.0315,0.0605};// correcting dipole, short deflection
MDSH_tt66: RBEND, L := l.MDSH,	APERTYPE = ELLIPSE, APERTURE = {0.0605,0.0315};// correcting dipole, short deflection
MDLV: RBEND, L := l.MDLV,	APERTYPE = ELLIPSE, APERTURE = {0.0315,0.0605};// correcting dipole, long deflection
MDLH: RBEND, L := l.MDLH,	APERTYPE = ELLIPSE, APERTURE = {0.0605,0.0315};// correcting dipole, long deflection
MDAV: RBEND, L := l.MDAV,	APERTYPE = ELLIPSE, APERTURE = {0.043,0.0615};// correcting dipole, enlarged aperture

//---------------------- OTHERS          --------------------------------------

TED:  INSTRUMENT, L := l.TED,	APERTYPE = ELLIPSE, APERTURE = {0.050,0.050};  // beam dump



// TT60COM
abiv.610013  =  0.0;
abih.610104  =  0.00074388;
abih.610206  =  0.00075584;
abiv.610304  = -0.00050465;
abih.610337  =  0.0;
abib.610405  =  0.00827710;
abie.610405  = -0.00839623;
abib.610523  =  0.00827690;
// TT66
abie.660224  = -1.05632e-04;
abis.660004  =  0.00293080;
abib.660107  =  5.98200e-03;

t9.frontsurface.at = 4.13780e+02;
l.TT66 = 428;
!offset =-7.27;

option, warn;
option, echo;
return;
