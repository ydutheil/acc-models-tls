/******************************************************************
 **
 **  PS injection: LHC INDIV
 **
 **  S. Ogur, W. Bartmann, M. Fraser and F. Velotti:
 **  Caluclate initial conditions for PS injection
 **
 ******************************************************************/


/******************************************************************
 * Call lattice files
 ******************************************************************/

! Path needs updating when moving into repository

option, -warn;
call, file = "ps_repo/ps_mu.seq";
call, file = "ps_repo/ps_ss.seq";
call, file = "ps_repo/scenarios/lhcindiv/0_injection/ps_inj_lhcindiv.str";


/*******************************************************************************
 * Beam command
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, sequence=ps, PC = 2.794987;
 BRHO := BEAM->PC * 3.3356;
 
/******************************************************************
* Determine injection geometry and install markers
******************************************************************/

pibsg52marker: marker;

SEQEDIT, SEQUENCE=PS;
       	FLATTEN;
        INSTALL, ELEMENT=pibsg52marker, AT= 321.655835 , FROM=PS$START;
        FLATTEN;
ENDEDIT;


 /******************************************************************
 * Compute PS injection reference trajectory (to be updated with SU (GEODE) measured values) 
 ******************************************************************/

calculate_injection(kfa45kick) : macro = {

use, sequence=ps;
select, flag=twiss, column=name,keyword,l,s,K0L,K1L,K2L,K3L,x,px,y,py,betx,alfx,bety,alfy,dx,dpx,dy,dpy,mux,muy,angle,K0L,K1L,K2L,K2S,K3L,K3S,VKICK,HKICK;
twiss, sequence=ps, centre = true;

! Invert s-coordinate direction
BETA0INV:  BETA0,
      BETX = table(twiss,pibsg52marker,betx),
      ALFX = -1*table(twiss,pibsg52marker,alfx),
      MUX  = table(twiss,pibsg52marker,mux),
      BETY = table(twiss,pibsg52marker,bety),
      ALFY = -1*table(twiss,pibsg52marker,alfy),
      MUY  = table(twiss,pibsg52marker,muy),
      X    = table(twiss,pibsg52marker,x),
      PX   = -1*table(twiss,pibsg52marker,px),
      Y    = table(twiss,pibsg52marker,y),
      PY   = -1*table(twiss,pibsg52marker,py),
      T    = table(twiss,pibsg52marker,t),
      PT   = table(twiss,pibsg52marker,pt),
      DX   = table(twiss,pibsg52marker,dx),
      DPX  = -1*table(twiss,pibsg52marker,dpx),
      DY   = table(twiss,pibsg52marker,dy),
      DPY  = -1*table(twiss,pibsg52marker,dpy);

! Create a sequence to compute beam parameters at the end of SMH42
EXTRACT, SEQUENCE=PS, FROM=PI.SMH42.ENDMARKER, TO=pibsg52marker,NEWNAME=PSINJ;

SEQEDIT, SEQUENCE=PSINJ;
       	FLATTEN;
        REFLECT;
        FLATTEN;
ENDEDIT;

! Turn on extraction kicker:
KPIKFA45 = kfa45kick;

use, sequence=psinj;
select, flag=twiss, column=name,keyword,l,s,K0L,K1L,K2L,K3L,x,px,y,py,betx,alfx,bety,alfy,dx,dpx,dy,dpy,mux,muy,angle,K0L,K1L,K2L,K2S,K3L,K3S,VKICK,HKICK;
twiss, sequence=psinj, centre=true, beta0 = BETA0INV;

BETA0INJ:  BETA0,
      BETX = table(twiss,PI.SMH42.ENDMARKER,betx),
      ALFX = -1*table(twiss,PI.SMH42.ENDMARKER,alfx),
      MUX  = table(twiss,PI.SMH42.ENDMARKER,mux),
      BETY = table(twiss,PI.SMH42.ENDMARKER,bety),
      ALFY = -1*table(twiss,PI.SMH42.ENDMARKER,alfy),
      MUY  = table(twiss,PI.SMH42.ENDMARKER,muy),
      X    = table(twiss,PI.SMH42.ENDMARKER,x),
      PX   = -1*table(twiss,PI.SMH42.ENDMARKER,px),
      Y    = table(twiss,PI.SMH42.ENDMARKER,y),
      PY   = -1*table(twiss,PI.SMH42.ENDMARKER,py),
      T    = table(twiss,PI.SMH42.ENDMARKER,t),
      PT   = table(twiss,PI.SMH42.ENDMARKER,pt),
      DX   = table(twiss,PI.SMH42.ENDMARKER,dx),
      DPX  = -1*table(twiss,PI.SMH42.ENDMARKER,dpx),
      DY   = table(twiss,PI.SMH42.ENDMARKER,dy),
      DPY  = -1*table(twiss,PI.SMH42.ENDMARKER,dpy);

xpsinj = BETA0INJ->x;
value, xpsinj;
pxpsinj = BETA0INJ->px;
value, pxpsinj;

};
