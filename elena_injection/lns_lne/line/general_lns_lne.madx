!==============================================================================================
! MADX file for LNS-LNE optics
!
! M.A. Fraser, D. Gamba, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "LNS-LNE optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../../elena_extraction/lne lne_repo";
system, "ln -fns ./../../delnslni delnslni_repo";


/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6,eyn=4E-6;
 
/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";


/*****************************************************************************
 * LNS
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "delnslni_repo/lns/lns.ele";
 call, file = "delnslni_repo/lns/lns_lne_k.str";
 call, file = "delnslni_repo/lns/lns.seq";
 !call, file = "delnslni_repo/lns/lns.dbx"; !Presently no aperture database: to be updated
 option, echo;
 
 /*****************************************************************************
 * LNE00
 *
 *****************************************************************************/
 option, -echo;
 call, file = "lne_repo/lne00/lne00.ele";
 call, file = "lne_repo/lne00/lne00.seq";
 !call, file = "lne_repo/lne00/lne00.dbx"; !Presently no aperture database: to be updated
 option, echo;
 
 lns.zdsia.0030.E: marker;
 
 SEQEDIT, SEQUENCE=LNE00;
       	FLATTEN;
        INSTALL, ELEMENT=lns.zdsia.0030.E, AT= -0.3, FROM=lns.zdsia.0030;
        FLATTEN;
 ENDEDIT;
  
 EXTRACT, SEQUENCE=lne00, FROM=lne.start.0000, TO=lns.zdsia.0030.E, NEWNAME=lne00tolnssurvey;
 EXTRACT, SEQUENCE=lne00, FROM=lne.start.0000, TO=lns.zdsia.0030.E, NEWNAME=lne00tolns;
 
 ! Redfine ion switch for a deflection to the right:
 LNS.ZDSIA.0030_43.00: EXSIA_43;
 
 ! Redfine extraction kicker for a deflection to the right:
 lnr.zdfa.0610: exbendr220;
 
 SEQEDIT, SEQUENCE=lne00tolns; REFLECT; FLATTEN; ENDEDIT;
 
 /*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
 lnslne00: sequence, refer=ENTRY, l = 2.8264 + 4.4628;
   lns                         , at =      0;
   lne00tolns                  , at = 2.8264;
 endsequence;

 SEQEDIT, SEQUENCE=lnslne00; FLATTEN; ENDEDIT;
 
 
/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;


/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "../stitched/elena_source.inp";


! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


use, sequence= lnslne00;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0;


/*******************************************************************************
! set matched twiss parameters
 *******************************************************************************/
call, file = "../stitched/elena_matched.inp";
 
set_final_conditions() : macro = {

    INITBETA1: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_final_conditions();


 /*******************************************************************************
 * MATCH
 *******************************************************************************

! Limits for k of quads assuming hard-edge model for a voltage of 6.5kV: 
! LNE = 72.2 (length = 100 mm, electrode radius = 30 mm)
! LNS = 103.9 (length = 70 mm, electrode radius = 25 mm)

 lnslnematch: macro={
select,flag=twiss,column=name,s,betx,bety,alfx,alfy,dx,dpx,dy,dpy,mux,muy,l,k0l,k1l;
OPTION, sympl = false;
twiss, beta0=initbeta0, file = "twiss_lns_lne_match.tfs";
};

use, sequence= lnslne00;
match,vlength=false,use_macro;

emith = 6 ;
emitv = 4 ;
dppi = 2.5;

vary , NAME=KLNS.ZQSF.0001,step=1e-6,lower=-103,upper=103;
vary , NAME=KLNS.ZQSD.0002,step=1e-6,lower=-103,upper=103;

vary , NAME=KLNS.ZQMF.0020,step=1e-6,lower=-72,upper=72;
vary , NAME=KLNS.ZQMD.0021,step=1e-6,lower=-72,upper=72;

vary , NAME=klne.zqmf.0006,step=1e-6,lower=-72,upper=72;
vary , NAME=klne.zqmd.0007,step=1e-6,lower=-72,upper=72;
vary , NAME=klne.zqmf.0013,step=1e-6,lower=-72,upper=72;
vary , NAME=klne.zqmd.0014,step=1e-6,lower=-72,upper=72;


use_macro,name=lnslnematch;

alfxM = INITBETA1->alfx;
betxM = INITBETA1->betx;
alfyM = INITBETA1->alfy;
betyM = INITBETA1->bety;
dD := (table(twiss,lnslne00$END,dx) - INITBETA1->dx)*beta;
dDP := (table(twiss,lnslne00$END,dpx) - INITBETA1->dpx)*beta;

MMbetax := 0.5*(table(twiss,lnslne00$END,betx)*(1+alfxM^2)/betxM + betxM*(1+table(twiss,lnslne00$END,alfx)^2)/table(twiss,lnslne00$END,betx) - 2*alfxM*table(twiss,lnslne00$END,alfx));

MMbetay := 0.5*(table(twiss,lnslne00$END,bety)*(1+alfyM^2)/betyM + betyM*(1+table(twiss,lnslne00$END,alfy)^2)/table(twiss,lnslne00$END,bety) - 2*alfyM*table(twiss,lnslne00$END,alfy));

MMDx := 1 + 0.5*(dD^2 + (betxM*dDP + alfxM*dD)^2)*dppi^2/(betxM*emith/6/beta/gamman);

constraint, weight=1,range=#e, expr = MMbetax = 1;
constraint, weight=1,range=#e, expr = MMbetay = 1;
constraint, weight=1,range=#e, expr = MMDx = 1;

simplex,calls=10000,tolerance=1e-12;

endmatch;


 *******************************************************************************/
  

/*******************************************************************************
 * Twiss
 *******************************************************************************/
 
use, sequence= lns;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lns.tfs";

use, sequence= lnslne00;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lns_lne_nom.tfs";

/*******************************************************************************/

/*************************************
* Survey
*************************************/
 set_su_ini_conditions(xx) : macro = {

 x00 = table(survey,xx,X);
 y00 = table(survey,xx,Y);
 z00 = table(survey,xx,Z);
 theta00 = table(survey,xx,THETA);
 phi00 = table(survey,xx,PHI);
 psi00 = table(survey,xx,PSI);

 };
 
call, file = "./make_survey_lns_lne.madx"; 

/***********************************
* Cleaning up
***********************************/
system, "rm lne_repo";
system, "rm delnslni_repo";

stop;
